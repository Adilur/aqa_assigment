package org.serenitybdd.starter.stepdefinitions;


import static net.serenitybdd.rest.SerenityRest.lastResponse;

import API.Search.Models.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.response.Response;
import org.junit.Assert;
import org.serenitybdd.starter.actions.ProductAction;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;
import java.util.List;

public class SearchStepDefinitions {
    @Steps
    ProductAction productAction;

    @When("User search for {word}")
    public void userSearchForProduct(String product) {
        productAction.searchForProduct(product);
    }

    @Then("and {word} displayed in the product list")
    public void andDisplayedInTheProductList(String productName) throws IOException {
        var products =  productAction.GetProductList(lastResponse());
        List<Product>  filteredProducts = products.stream().filter(p -> p.title.toLowerCase().contains(productName)).toList();
        Assert.assertTrue(String.format("Product %s not displayed in the product list", productName), filteredProducts.size() > 0);
    }

    @Then("User get {int} response code")
    public void userGetResponseCode(int code) {
        var responseCode = lastResponse().statusCode();
        Assert.assertEquals(responseCode, code);
    }


    @Then("user does see {string} error message")
    public void userDoesSeeErrorMessage(String expectedmessage) throws JsonProcessingException {
        var message = productAction.GetMessage(lastResponse());
        Assert.assertTrue("user will get the error message", message.detail.error);
        Assert.assertEquals(message.detail.message, expectedmessage);
    }


}
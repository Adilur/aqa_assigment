package org.serenitybdd.starter.actions;

import java.util.Arrays;
import java.util.List;


import API.Client.SearchApiClient;
import API.Search.Models.Message;
import API.Search.Models.Product;
import io.restassured.response.Response;



public class ProductAction {

    private static final String SearchEndPoint = "/search/demo/";
    private static final String  BaseURI="https://waarkoop-server.herokuapp.com/api/v1";


    public Response searchForProduct(String productName) {
        return SearchApiClient.getInstance().getProductsByProductName(productName);
    }

    public List<Product> GetProductList(Response response) {
        return response.then().extract().body().jsonPath().getList(".", Product.class);
    }

    public Message GetMessage(Response response)  {
        return response.then().extract().body().as(Message.class);
    }
}
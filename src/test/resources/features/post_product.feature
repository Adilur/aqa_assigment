Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios
  Scenario Outline: Search for available product: <product>
    When User search for <product>
    Then User get 200 response code
    And  and <product> displayed in the product list

    Examples:
      |product|
      |orange |
      |apple  |
      |pasta  |
      |cola   |

  Scenario: Search for unavailable product: carrot
    When User search for "carrot"
    Then user does see "Not found" error message
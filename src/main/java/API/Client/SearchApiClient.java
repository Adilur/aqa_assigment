package API.Client;

import io.restassured.response.Response;
import starter.api.specification.CommonRequestSpec;

public class SearchApiClient {
    private static final String ROUTE = "api/v1/search/demo/";
    private static final SearchApiClient INSTANCE = new SearchApiClient();

    public static SearchApiClient getInstance() {
        return INSTANCE;
    }
    public Response getProductsByProductName(String productName) {
        return CommonRequestSpec.requestSpecification().get(String.format("%s%s", ROUTE, productName));
    }
}

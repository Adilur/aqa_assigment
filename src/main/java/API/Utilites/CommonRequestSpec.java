package starter.api.specification;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class CommonRequestSpec {
    private static final EnvironmentVariables environmentVariables = Injectors.getInjector()
            .getInstance(EnvironmentVariables.class);

    private static RestAssuredConfig getRestAssuredConfig(){
        return RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (type, s) -> {
                    ObjectMapper objectMapper = new ObjectMapper();
                    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    return objectMapper;
                }
        ));
    }

    public static RequestSpecification requestSpecification() {
        String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("baseurl");
        return SerenityRest.rest()
                .baseUri(baseUrl)
                .config(getRestAssuredConfig())
                .contentType(ContentType.JSON);
    }
}
# Serenity Cucumber Test Project

## 1. Development requirements

**Required:** Java Development Kit 20 (JDK20) and maven 3.8+ should be present on environment in order to develop and/or run the tests.

Also, https://projectlombok.org/ is used to simplify creating/editing Java POJOs (data models). So in order to compile Project from IDE Lombok plugin should be installed.

## 2. Configuring test project

Run this command to ensure the code has no errors:

```
$ mvn clean install -DskipTests=true
```
## 3. Writing new tests
1. Create new feature file or append existing one with new scenario in `src/test/resources/features` folder.
2. Implement a new action class or extend an existing one located in the `src/test/java/org/serenitybdd/starter/actions` folder. This class should contain methods that encapsulate the logic for specific actions in our case it's a logic related to products.
3. Implement a new step definition class or extend an existing one located in the `src/test/java/org/serenitybdd/starter/stepdefinitions` folder. This class should contain methods that link one or more Gherkin steps defined in step #1. Ensure that this class utilizes only methods from the classes created in step #2.
4. Implement API client class or extend an existing one in `src/main/java/api/client` that will be used in step#2.
5. Implement models for request/response or use existing one

## 4. Running tests
To trigger tests execution:
```
$ mvn clean verify -Devnironments=default
```
* ***Env:*** `-Devnironments={env}` where `{env}` is the environment name from `serenity.conf` file

## 5. Gitlab CI
Gitlab CI have 2 jobs:

1. execute_api_tests
2. publish_report

`execute_QA_tests` it's used for running test

`publish_QA_report` it's used for publish serenity report to gitlab pages. You could find report in 
Settings > Pages or in Deployment > Pages depend on what gitlab Ui you are using.

## 6. Changes
1. Removed all Gradle configuration files, as Maven is now the designated build tool.
2. Refactored pom.xml file, updating all dependency versions and adding a new ones.
3. Remove serenity.properties, uses serenity.conf instead. Remove properties related to webdriver because we use only API.
4. Implement common request specification class that functions as a standardized template for implementing API services across the entire framework
5. Implement API service specifically designed for product searches, serving as a centralized location for handling all requests related to searching for products
6. Implement tests parallel execution
7. Configure continuous integration (CI) in GitLab to save the report as an artifact and enable publishing them to GitLab Pages.
